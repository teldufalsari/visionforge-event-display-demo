package canvas

import emotion.react.css
import org.w3c.dom.Element
import react.*
import react.dom.html.ReactHTML.div
import space.kscience.dataforge.context.Context
import space.kscience.dataforge.context.request
import space.kscience.dataforge.names.Name
import space.kscience.visionforge.solid.Solid
import space.kscience.visionforge.solid.specifications.Canvas3DOptions
import space.kscience.visionforge.solid.three.ThreeCanvas
import space.kscience.visionforge.solid.three.ThreePlugin
import web.cssom.pct
import web.cssom.vh
import web.cssom.vw
import web.html.HTMLDivElement

external interface ThreeCanvasProps : Props {
    var context: Context
    var options: Canvas3DOptions?
    var solid: Solid?
    var selected: Name?
}

/**
 * This React component is analogous to the class from VisionForge with the same name.
 * I keep it here in case imports are broken (they are broken at the time i write this).
 */
val ThreeCanvasComponent = FC<ThreeCanvasProps> { props ->
    val elementRef = useRef<Element>(null)
    var canvas by useState<ThreeCanvas?>(null)

    val three: ThreePlugin = useMemo(props.context) { props.context.request(ThreePlugin) }

    useEffect(props.solid, props.options, elementRef) {
        if (canvas == null) {
            val element = elementRef.current ?: error("Canvas element not found")
            canvas = ThreeCanvas(three, element, props.options ?: Canvas3DOptions())
        }
    }

    useEffect(canvas, props.solid) {
        props.solid?.let { obj ->
            canvas?.render(obj)
        }
    }

    useEffect(canvas, props.selected) {
        canvas?.select(props.selected)
    }

    div {
        css {
            maxWidth = 100.vw
            maxHeight = 100.vh
            width = 100.pct
            height = 100.pct
        }
        ref = elementRef as Ref<HTMLDivElement>
    }
}
