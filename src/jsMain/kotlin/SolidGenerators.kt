import space.kscience.visionforge.Colors
import space.kscience.visionforge.solid.*
import kotlin.random.Random

fun generateEvents(radius: Float): SolidGroup {
    val count = Random.nextInt(10, 20)
    return SolidGroup {
        repeat(count) {
            sphere(radius, it.toString()) {
                x = 5.0 * (Random.nextFloat() - 0.5)
                y = 2.0 * (Random.nextFloat() - 0.5)
                z = 2.0 * (Random.nextFloat() - 0.5)
                color(Colors.red)
            }
        }
    }
}

fun generateGeometry(distance: Float): SolidGroup {
    return SolidGroup {
        box(10, 3, 3, "Magnet1") {
            x = 0.0
            y = -distance
            z = 0.0
            color(Colors.gray)
        }
        box(10, 3, 3, "Magnet2") {
            x = 0.0
            y = distance
            z = 0.0
            color(Colors.gray)
        }
    }
}
