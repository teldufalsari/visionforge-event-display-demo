import canvas.ThreeCanvasComponent
import react.FC
import react.Props
import react.useEffect
import space.kscience.dataforge.context.Context
import space.kscience.visionforge.setAsRoot
import space.kscience.visionforge.solid.Solid
import space.kscience.visionforge.visionManager

external interface EventViewProps: Props {
    var displayedVision: Solid?
    var context: Context
}

val EventView = FC<EventViewProps> { props ->
    ThreeCanvasComponent {
        solid = props.displayedVision
        context = props.context
    }
    useEffect(props.displayedVision) {
        props.displayedVision?.setAsRoot(props.context.visionManager)
    }
}
