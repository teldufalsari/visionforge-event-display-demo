import web.cssom.*
import emotion.react.Global
import emotion.react.css
import emotion.react.styles
import react.*
import react.dom.html.ReactHTML.div
import react.dom.html.ReactHTML.input
import space.kscience.dataforge.context.Context
import space.kscience.dataforge.names.parseAsName
import space.kscience.visionforge.Colors
import space.kscience.visionforge.solid.*
import space.kscience.visionforge.solid.three.ThreePlugin
import web.html.InputType
import kotlin.random.Random

// Context is required to observe Vision updates (i.e. to allow
// automatic updates of corresponding Object3D when we update Visions)
val viewContext = Context {
    plugin(Solids)
    plugin(ThreePlugin)
}

// Names used as keys to access and update Visions
// Refer to DataForge documentation for more details
val EVENTS_NAME = "DEMO_EVENTS".parseAsName(false)
val GEOMETRY_NAME = "DEMO_GEOMETRY".parseAsName(false)


val EventDisplay = FC<Props> {
    // Global CSS rules
    Global {
        styles {
            "html,\n" +
            "body" {
                height = 100.vh
                width = 100.vw
                margin = 0.px
            }
            "body > div" {
                display = Display.flex
                height = 100.vh
                width = 100.vw
                flexDirection = FlexDirection.column
                justifyContent = JustifyContent.start
                alignItems = AlignItems.center
            }
            "*,\n" +
            "*:before,\n" +
            "*:after" {
                boxSizing = BoxSizing.borderBox
            }
        }
    }

    // Values that represent raw data, which can be converted into Visions
    var eventData: kotlin.Float? by useState(null)
    var geometryData: kotlin.Float? by useState(null)

    // Main Vision containing the entire event display scene
    val containedVision: SolidGroup by useState(SolidGroup {
        ambientLight {
            color(Colors.white)
        }
    })

    // Hooks that update main Vision when new raw data is loaded
    useEffect(eventData) {
        eventData?.let {
            containedVision.setChild(EVENTS_NAME, generateEvents(it))
        }
        if (eventData == null) {
            containedVision.setChild(EVENTS_NAME, null)
        }
    }
    useEffect(geometryData) {
        geometryData?.let {
            containedVision.setChild(GEOMETRY_NAME, generateGeometry(it))
        }
        if (geometryData == null) {
            containedVision.setChild(GEOMETRY_NAME, null)
        }
    }

    div {
        css {
            display = Display.flex
            flexDirection = FlexDirection.column
            alignItems = AlignItems.center
            height = 100.pct
            width = 100.pct
        }
        div {
            css {
                display = Display.flex
                flexDirection = FlexDirection.row
                alignItems = AlignItems.center
                justifyContent = JustifyContent.center
                width = 100.pct

            }
            input {
                css {
                    margin = 5.px
                    padding = 5.px
                }
                type = InputType.button
                value = "Update Events"
                onClick = {
                    eventData = 2f * (Random.nextFloat() - 0.5f)
                }
            }
            input {
                css {
                    margin = 5.px
                    padding = 5.px
                }
                type = InputType.button
                value = "Update Geometry"
                onClick = {
                    geometryData = 4f + 2f * Random.nextFloat()
                }
            }
            input {
                css {
                    margin = 5.px
                    padding = 5.px
                    backgroundColor = NamedColor.lightcoral
                    color = NamedColor.white
                }
                type = InputType.button
                value = "Clear Scene"
                onClick = {
                    geometryData = null
                    eventData = null
                }
            }
        }
        div {
            css {
                width = 98.pct
                height = 1.pct
                flexGrow = number(1.0)
                margin = 5.px
                display = Display.flex
                justifyContent = JustifyContent.center
                alignItems = AlignItems.center
            }
            EventView {
                displayedVision = containedVision
                context = viewContext
            }
        }
    }
}
